<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?><!DOCTYPE html>

<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->

<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->

<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->

<!--[if IE 9]>
<html id="ie9" <?php language_attributes(); ?>>
<![endif]-->

<!--[if !(IE 6) | !(IE 7) | !(IE 8) | !(IE 9) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php if(is_search()){esc_html(bloginfo('name')); }else{ wp_title('');} ?></title>



<?php /*got typekit?*/ ?>
<script src="//use.typekit.net/acl5wmu.js"></script>
<script>try{Typekit.load();}catch(e){}</script>

<?php /*got favicon?*/ ?>
<!--<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />-->

<?php /*stylesheets*/ ?>
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo( 'template_directory' ); ?>/print.css" />
 
<?php wp_head(); ?>

<?php /*if google jQuery is not available (called in wp_head), fallback to local copy*/ ?>
<script>window.jQuery || document.write('<script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery-1.8.2.min.js"><\/script>')</script>

<!--[if IE]>

<?php /*once we have jQuery, fix html5 for IE*/ ?>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/html5printshiv.js"><\/script>')</script>

<?php /*CSS3Pie is a powerful tool but requires a few steps to implement: http://css3pie.com/documentation/pie-js/ */ ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/css3pie/1.0.0/PIE.js"></script>

<?php /*enables things like last-child{} selection*/ ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>

<![endif]-->

</head>
 
<body <?php body_class(); ?>>

<?php //print only ?>
<img class="print-header" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/print-header.jpg" alt="<?php bloginfo('name'); ?>" />

<div id="blog-wrapper" class="blog-wrapper css3_pie">

	
	 <header id="blog-header" class="blog-header">
	 
	<div class="header-widgets">
	<?php dynamic_sidebar( 'header-widgets' ); ?>
	</div>
	
	
    <hgroup id="main_hgroup">
      <h1 id="blog-title" class="blog-title">
        <a class="hide-text" href="<?php echo esc_attr(home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
      </h1>
      <?php if ( get_bloginfo( 'description' ) ) : ?>
        <h2 id="blog-description" class="blog-description hide-text">
          <?php bloginfo( 'description' ); ?>
        </h2>
      <?php endif; ?>
    </hgroup>
    
  </header>
 

<div id="main-wrapper" class="main-wrapper">

<div id="main" class="main <?php if(is_archive() ) { echo ' grid '; } ?> ">


