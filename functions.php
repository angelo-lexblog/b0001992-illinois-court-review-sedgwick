<?php
/*CSS3 PIE
This is included here as a format reminder, as it's identical to what's in the parent theme.
If you need to CSS3PIE something in your project, try to apply the class .css3_pie to that
element in the markup.  Failing that, add the appropriate selector to the jQuery below and
uncomment the call to add action.*/


function lxb_base_child_css3_pie(){
?>
	<!--[if lt IE 10]>
	<script>
		jQuery(function() {
	    	if (window.PIE) {
	    	    jQuery('*** YOUR SELECTOR HERE ***').each(function() {
			         PIE.attach(this);
			    });
			}
		});
	</script>
	<![endif]-->
<?php
}

//add_action('wp_footer', 'lxb_base_child_css3_pie');

	function lxb_base_fix_some_stuff_on_load(){
	?>
	<script>		
	
		jQuery( window ).load(function() {

			<?php /* make sidebar min-height of main column */ ?>	
			var main_height = jQuery('.main-wrapper .main').height() +48;
			//alert('main height:' + main_height);
			jQuery('.main-wrapper .sidebar').css('min-height', main_height);
		
			
		});		
		
	</script>
	<?php
	}

add_action('wp_footer', 'lxb_base_fix_some_stuff_on_load');

function lxb_base_comments_link(){
		
		global $post;
		
		if ( comments_open() ) {
			
			comments_popup_link( 
    			__( '<i class="icon-comment"></i>', 'twentytwelve' ), 
    			__( '<i class="icon-comment"></i>', 'twentytwelve' ), 
    			__( '<i class="icon-comment"></i>', 'twentytwelve' ),
    			'post-comments',
    			__( '', 'twentytwelve' )
			);
			
		}
	}
	
	
		function lxb_base_print_screen(){
	
		$print=esc_html__('', 'twentytwelve');
	
		$out ='<a class="post-print" href="javascript:window.print()"><i class=" icon-print"></i>'.$print.'</a>';
		
		return $out;
	}
function lxb_base_if_more_pages(){

		global $wp_query;
		global $post;
		$out ='';
	
		// Display navigation to next/previous pages when applicable
		if (  $wp_query->max_num_pages > 1 ) {
	
			//I18n
			$prev_text = esc_html__('Newer Posts', 'twentytwelve');
			$next_text = esc_html__('Older Posts', 'twentytwelve');
	
			//the class "unicode char should be treated with a unicode font such as font-family: 'lucida sans unicode' to prevent breakage in ie
			$right_arrow = ' <i class="icon-chevron-right"></i>';
			$left_arrow = '<i class="icon-chevron-left"></i> ';
	
			//return html anchor tags
			$prev = get_previous_posts_link($left_arrow.$prev_text);
			$next = get_next_posts_link($next_text.$right_arrow);
	
			
			$out="
				<nav class='pagination'>
					<span class='next'>$next</span>
					<span class='prev'>$prev</span>
				</nav>
			";
		}
		return $out;
	}



?>